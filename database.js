const mongo = require('mongoose');

(async () => {
    try {
        const db = await mongo.connect("mongodb://localhost:27017/MiDulceCaramelo-pro4");
        console.log("Conexion establecida en: " + db.connection.name);
    } catch (error) {
        console.error(error);
    }
})();
