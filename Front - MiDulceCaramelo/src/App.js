import React, {Fragment} from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "../src/pages/auth/Login";
import NewAccount from "../src/pages/auth/NewAccount";
import ProdList from "./managers/adm/ProdList";
import NewProduct from "./managers/adm/NewProduct";
import OrderList from "./managers/adm/OrderList";
import UpdateProduct from "./managers/adm/UpdProduct";
import Home from "./pages/home/index";
import Products from "./pages/products/index";
import Detalle from "./pages/products/view";
import Cotizar from "./pages/cot/formCot";



function App() {
  return (
    <Fragment>
        <Router>
            <Routes>
                <Route path="/" exact element={<Home/>}/>
                <Route path="/Login" exact element={<Login/>}/>
                <Route path="/NewAccount" exact element={<NewAccount/>}/>
                <Route path="/Home" exact element={<Home/>}/>
                <Route path="/Admin/ProdList" exact element={<ProdList/>}/>
                <Route path="/Admin/NewProduct" exact element={<NewProduct/>}/>
                <Route path="/Admin/UpdateProduct/:id" exact element={<UpdateProduct/>}/>
                <Route path="/Admin/OrderList" exact element={<OrderList/>}/>
                <Route path="/products" exact element={<Products/>}/>
                <Route path="/products/detalle/:id" exact element={<Detalle/>}/>
                <Route path="/products/cotizar/:id" exact element={<Cotizar/>}/>
            </Routes>
        </Router>
    </Fragment>
  );
}

export default App;
