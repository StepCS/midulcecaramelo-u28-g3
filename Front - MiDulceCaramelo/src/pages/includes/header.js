import React from "react";
import { Link } from "react-router-dom";

const Header = () => {

    return (
        <div class="header__dashboard">
            <div class="header__left">
                <div className="header__logo">
                    <a href="home.html">
                        <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhIf81NburFfqoGMwXx_FVlQsxQBKpQSBwSQ&usqp=CAU"
                            alt="logo"
                        />{" "}
                    </a>
                </div>
            </div>

            <div className="header__meta">
                <ul className="meta__item d-xl-block d-none">
                    <li><i className="fa-solid fa-location-dot" />Santander, Colombia</li>
                </ul>
                <div className="miniCart">
                    <div className="header__cart">
                    </div>
                </div>
            </div>            

        </div>


    )
}
export default Header;