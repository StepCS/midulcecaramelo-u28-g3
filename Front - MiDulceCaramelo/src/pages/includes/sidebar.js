import React from "react";
import { Link } from "react-router-dom";

const Slidebar = () => {

    return (

        <div className="dashboard__mainWrapper">
        {/* Main SideBar Start */}
        <div className="flyOut-sidebar">
            <div id="sideBar" className>
                {/* Main Logo Start */}
                <div className="main__logo">
                    <a href="dashboard.html" className="brand__logo">
                        <img src="https://lms.uis.edu.co/mintic2022/pluginfile.php/1/theme_edumy/headerlogo1/1663168415/MisionTIC-UIS.png" />
                    </a>
                    <div id="toggle__btn">
                        <svg width={24} height={24} viewBox="0 0 24 24" fill="none">
                            <path d="M22 12H3" stroke="#000000" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M22 4H13" stroke="#000000" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M22 20H13" stroke="#000000" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M7 7L2 12L7 17" stroke="#000000" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>
                </div>
                {/* Main Logo End */}
                {/* NavItem Start */}
                <ul className="nav__item">
                    <li className="nav__heading">Mi Dulce Caramelo</li>
                    <li className="nav__list active">
                        <a href="d-product-list.html">
                            <div className="icon">
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 17L12 22L22 17M2 12L12 17L22 12M12 2L2 7L12 12L22 7L12 2Z" stroke="#667085" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </div>
                            <Link className="text-muted" to={`/Admin/ProdList`}>Todos los productos</Link>
                        </a>
                    </li>
                    {/* Navlist Start */}
                    <li className="nav__list">
                        <a href="d-order-list.html">
                            <div className="icon">
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 11L12 14L22 4M21 12V19C21 19.5304 20.7893 20.0391 20.4142 20.4142C20.0391 20.7893 19.5304 21 19 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V5C3 4.46957 3.21071 3.96086 3.58579 3.58579C3.96086 3.21071 4.46957 3 5 3H16" stroke="#667085" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </div>
                            <Link className="text-muted" to={`/Admin/ProdList`}>Ordenes</Link>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


        
    )
}
export default Slidebar;