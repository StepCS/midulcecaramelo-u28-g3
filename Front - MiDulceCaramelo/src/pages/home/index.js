import React from "react";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { Link } from "react-router-dom";
import Header from "../includes/header";
import Sidebar from "../includes/sidebar";
import Footer from "../includes/footer";

const api = new APIInvoke();

const Home = () => {

    const [products, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])
    

    return (

        <div>

            <Header />
            <Sidebar />

            <main>
                <section className="banner__ads__section ">
                    <div className="main__container ">
                        <div className="row">
                            <div className="col-12">
                                <div className="banner__ads__main">
                                    {
                                        products.map(
                                            product =>
                                                <div className="banner__ads__wrapper mb-3">
                                                    <div className="banner__ads__single__item">
                                                        <span className="banner__ads__subtitle">Producto</span>
                                                        <h5 className="banner__ads__title text-wrap ">
                                                        {product.name}
                                                        </h5>
                                                        <div className="shop__btn">
                                                            <Link className="text-danger" to={`/products`}>Todos los productos</Link>
                                                        </div>
                                                    </div>
                                                    <div className="banner__ads__image">
                                                        <img src={product.img} alt={product.name} />
                                                    </div>
                                                </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <Footer />
        </div>
    )
}
export default Home;