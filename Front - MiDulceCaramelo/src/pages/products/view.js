import React from "react";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Header from "../includes/header";
import Sidebar from "../includes/sidebar";
import Footer from "../includes/footer";

const api = new APIInvoke();

const Detalle = () => {

    const [item, setProduct] = useState([]);

    const loadProduct = async (id) => {
        const response = await api.invokeGET("/api/Product/list/" + id);
        console.log(response);
        setProduct(response);
    }

    const param = useParams();

    useEffect(() => {
        loadProduct(param.id);
    })
    return (

        <div >

            <Header />
            <Sidebar />

            <section className="product-main">
                <div className="main__container">
                    <div className="row product-detail">
                        <div className="col-md-6">
                            <div className="product-gallery">
                                <div className="product-gallery__main swiper productGallerySwiper">
                                    <div className="swiper-wrapper">
                                        <div className="swiper-slide">
                                            <div className="gallery-item">
                                                <img src={item.img} alt={item.name} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="product-detail__wrapper">
                                <h2 className="product-detail__title">{item.name}</h2>
                                <h3 className="product-detail__price">${item.price}</h3>
                                <h3 className="product-detail__price">{item.category}</h3>
                                <p className="product-detail__short_desc">
                                    {item.description}
                                </p>
                            </div>
                            <div className="cart__btn">
                                <Link className="btn btn-outline" to={`/products/cotizar/${item._id}`}>Cotizar</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <Footer />

        </div >


    )
}
export default Detalle;