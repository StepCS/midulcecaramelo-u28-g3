import React from "react";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import Header from "../includes/header";
import Sidebar from "../includes/sidebar";
import Footer from "../includes/footer";
import { Link } from "react-router-dom";
const api = new APIInvoke();

const Products = () => {

    const [products, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])

    const onSubmit = async (e, id) => {
        e.preventDefault();
        const param = document.getElementById("param").value;
        const consult = await api.invokeGET("/api/Product/list/" + param);
        if (consult.mensaje === "sin registros") {
            document.getElementById("param").value = "";
        }
        else {
            setProduct(consult);
            document.getElementById("param").value = "";
        }
    }
    return (

        <div >

            <Header />
            <Sidebar />

            <section className="archive-category">
                <div className="main__container">                    
                    <div className="row archive-category__inner">
                        <div className="product-card__wrapper">                           
                            {
                                products.map (
                                    product => 
                                    <div className="product-card">
                                        <div className="product__image__wrapper">
                                            <a to={`/products/${product._id}`} className="product__image">
                                                <img src={product.img} alt={product.name} />
                                            </a>
                                        </div>
                                        <div className="product__content">
                                            <div className="product__title">
                                                <h5><a href="product-single.html">{product.name}</a></h5>
                                            </div>
                                            <div className="product__bottom">
                                                <div className="product__price">
                                                    ${product.price}
                                                </div>
                                                <div className="cart__action__btn">
                                                    <div className="cart__btn">
                                                        <Link className="btn btn-outline" to = {`/products/detalle/${product._id}`}>Detalle</Link> 
                                                    </div>
                                                    <div className="cart__btn">
                                                        <Link className="btn btn-outline" to = {`/products/cotizar/${product._id}`}>Cotizar</Link> 
                                                    </div>
                                                    <div className="quantity cart__quantity">
                                                        <button type="button" className="decressQnt">
                                                            <span className="bar" />
                                                        </button>
                                                        <form onSubmit ={onSubmit}>
                                                        <input className="qnttinput" type="number" disabled  />
                                                        <button type="button" className="incressQnt">
                                                            <span className="bar" />
                                                        </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}    
                                    
                        </div>
                    </div>
                </div>
            </section>

            <Footer />

        </div>


    )
}
export default Products;