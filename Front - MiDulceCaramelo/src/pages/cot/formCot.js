import React from "react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { APIInvoke } from "../../utils/ApiInvoke";
import { useParams } from "react-router-dom";
import Header from "../includes/header";
import Sidebar from "../includes/sidebar";
import Footer from "../includes/footer";

const api = new APIInvoke();

const Cotiza = () => {

    const [item, setItem] = useState({
        address: "",
        city: "",
        nameUs: "",
        phone: "",
        producto: "",
        amount: "",
        total: ""

    });

    const { address, city, nameUs, phone, amount, total } = item;

    const onChange = (e) => {
        setItem({
            ...item,
            [e.taget.name]: e.taget.value
        })
    }

    const newCoti = async () => {

        const data = {
            address: item.address,
            city: item.city,
            nameUs: item.nameUs,
            phone: item.phone,
            producto: item.producto,
            amount: item.amount,
            total: item.total
        }

        const response = await api.invokePOST("/api/Cotizacion/new", data);
        console.log(response);

    }

    const loadProduct = async (id) => {
        const response = await api.invokeGET("/api/Product/list/" + id);
        console.log(response);
        setItem(response);
    }

    const pay = async (amount) => {
        return item.price * amount
    }

    const param = useParams();

    useEffect(() => {
        document.getElementById("name").focus();
        loadProduct(param.id);
    });

    const onSubmit = (e) => {
        e.preventDefault();
        newCoti();
    }


    return (

        <div >

            <Header />
            <Sidebar />

            <section className="product-main">
                <div className="main__container">
                    <div className="row product-detail">
                        <div className="col-md-6">
                            <div className="product-gallery">
                                <div className="product-gallery__main swiper productGallerySwiper">
                                    <div className="swiper-wrapper">
                                        <div className="swiper-slide">
                                            <div className="gallery-item">
                                                <img src={item.img} alt={item.name} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <form onSubmit={onSubmit} action="#" className="dashboard__form">
                                <div className="row">
                                    <div className="col-xxl-8">
                                        <div className="dashboard-block">
                                            <div className="dashboard-block__title">
                                                <h4>Información</h4>
                                            </div>
                                            <div className="form__wrapper form-padding">
                                                <div className="flex__form c-3">
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Nombre</label>
                                                        <input type="text" name="nameUs" id="nameUs" className="form-control" placeholder="Nombre completo" value={nameUs} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Dirección</label>
                                                        <input type="text" name="address" id="address" className="form-control" placeholder="Dirección" value={address} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Ciudad</label>
                                                        <input type="text" name="city" id="city" className="form-control" placeholder="Ciudad" value={city} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="stock">Celular</label>
                                                        <input type="number" name="phone" id="phone" className="form-control" placeholder="Telefono" value={phone} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="slug">Cantidad</label>
                                                        <input type="number" name="amount" id="amount" className="form-control" placeholder="Cantidad del producto" value={amount} onChange={onChange} required />
                                                    </div>
                                                    <div className="product-group">
                                                        <div className="icon-image">
                                                            <img src={item.img} alt={item.name} />
                                                        </div>
                                                        <div className="title">{item.idProd}</div>
                                                    </div>

                                                    <div className="form-group">
                                                        <label htmlFor="slug">Total</label>
                                                        <input type="number" name="Total" id="Total" className="form-control" placeholder="Cantidad del producto" value={pay} onChange={onChange} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="dashboard-block">
                                            <div className="btn__group flex-wrap">
                                                <Link className="btn btn-primary" type="submit" to={`/Admin/ProdList`}>Crear Producto</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div className="product-detail__wrapper">
                                <h2 className="product-detail__title">{item.name}</h2>
                                <h3 className="product-detail__price">${item.price}</h3>
                                <h3 className="product-detail__price">{item.category}</h3>
                                <p className="product-detail__short_desc">
                                    {item.description}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <Footer />

        </div >


    )
}
export default Cotiza;