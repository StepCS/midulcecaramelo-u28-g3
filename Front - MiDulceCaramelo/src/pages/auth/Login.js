import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { APIInvoke } from '../../utils/ApiInvoke';
import swal from 'sweetalert';
import { useNavigate } from 'react-router-dom';
const api = new APIInvoke();

const Login = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const naveg = useNavigate();

    const [adm, setAdmin] = useState({
        email: "",
        password: ""
    });
    const { email, password } = adm;

    const onChange = (e) => {
        setAdmin({
            ...adm,
            [e.target.name]: e.target.value
        })
    };

    useEffect(() => {
        document.getElementById("email").focus();
    }, []);

    const admLogin = async () => {

        const data = {
            email: adm.email,
            password: adm.password
        }
        const resp = await api.invokePOST("/api/UserAdm/login", data);
        console.log(resp);

        const access = resp.msg;
        let titulo, mensaje, tipo;
        if (access === "Ingreso") {
            titulo = "Proceso Exitoso!";
            mensaje = "Ingreso exitoso al sistema";
            tipo = "success";
            alerta(mensaje, tipo, titulo);

            localStorage.setItem("admin", resp.adm);

            naveg("/Home");
        }
        else if (access === "Denegado") {
            titulo = "Acceso Denegado";
            mensaje = "Usuario o contraseña incorrectos";
            tipo = "error";
            alerta(mensaje, tipo, titulo);
        }

        setAdmin({
            email: "",
            password: ""
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        admLogin();
    };

    return (

        <section class="fullPage-section p-0">
            <div class="full-page__wrapper">
                <div class="full-page__image">
                    <img src="https://mejorconsalud.as.com/wp-content/uploads/2017/10/4-Postres-caseros-nutritivos-y-con-muy-pocos-ingredientes-768x512.jpg" alt="comming-soon" />
                    <div class="contact__overlay">
                        <div class="contact__info">
                            <h2 class="section__title">Mi Dulce Caramelo</h2>
                            <p class="content">Ingreso administrativo.</p>
                            <div class="contact__meta">
                                <ul class="contact__meta__list">
                                    <li>
                                        <div class="icon">
                                            <svg width="26" height="32" viewBox="0 0 26 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M12.5817 16.9803C15.331 16.9803 17.5598 14.7515 17.5598 12.0021C17.5598 9.25278 15.331 7.02399 12.5817 7.02399C9.83231 7.02399 7.60352 9.25278 7.60352 12.0021C7.60352 14.7515 9.83231 16.9803 12.5817 16.9803Z"
                                                    fill="#0067FF"
                                                ></path>
                                                <path
                                                    d="M12.5818 0C5.6601 0 0 5.5919 0 12.5477C0 18.276 8.59243 28.2323 11.593 31.5397C12.1385 32.1534 13.0591 32.1534 13.6047 31.5397C16.6052 28.2323 25.1977 18.276 25.1977 12.5477C25.1636 5.52371 19.5035 0 12.5818 0ZM12.5818 20.9014C7.63772 20.9014 3.61428 16.878 3.61428 12.0021C3.61428 7.12627 7.63772 3.10282 12.5818 3.10282C17.5258 3.10282 21.5493 7.12627 21.5493 12.0021C21.5493 16.878 17.5258 20.9014 12.5818 20.9014Z"
                                                    fill="#0067FF"
                                                ></path>
                                            </svg>
                                        </div>
                                        <a href="tel:555-0129">3000000000</a>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <svg width="26" height="32" viewBox="0 0 26 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M12.5817 16.9803C15.331 16.9803 17.5598 14.7515 17.5598 12.0021C17.5598 9.25278 15.331 7.02399 12.5817 7.02399C9.83231 7.02399 7.60352 9.25278 7.60352 12.0021C7.60352 14.7515 9.83231 16.9803 12.5817 16.9803Z"
                                                    fill="#0067FF"
                                                ></path>
                                                <path
                                                    d="M12.5818 0C5.6601 0 0 5.5919 0 12.5477C0 18.276 8.59243 28.2323 11.593 31.5397C12.1385 32.1534 13.0591 32.1534 13.6047 31.5397C16.6052 28.2323 25.1977 18.276 25.1977 12.5477C25.1636 5.52371 19.5035 0 12.5818 0ZM12.5818 20.9014C7.63772 20.9014 3.61428 16.878 3.61428 12.0021C3.61428 7.12627 7.63772 3.10282 12.5818 3.10282C17.5258 3.10282 21.5493 7.12627 21.5493 12.0021C21.5493 16.878 17.5258 20.9014 12.5818 20.9014Z"
                                                    fill="#0067FF"
                                                ></path>
                                            </svg>
                                        </div>
                                        Colombia
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="full-page__inner">
                    <div class="full-page__content">
                        <div class="popup__form d-block">
                            <form action="#" class="form__wrapper p-0">
                                <div class="popup__title">
                                    <h2>Login</h2>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email"
                                        type="email"
                                        className="form-control"
                                        placeholder="Correo Electronico"
                                        name="email"
                                        value={email}
                                        onChange={onChange}
                                        required
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="pass">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        placeholder="Contraseña"
                                        name="password"
                                        value={password}
                                        onChange={onChange}
                                        required
                                    />
                                </div>
                                <div class="submit__btn">
                                    <button type="submit" class="btn btn-primary mt-0 w-100">Iniciar Sesión</button>
                                </div>
                                <div class="account__desc">
                                    <Link to={"/NewAccount"} className="btn btn-link mt-0 w-100">Crear Cuenta</Link>
                                </div>
                    </form>
                </div>
            </div>
        </div>
            </div >
        </section >
    )
}

export default Login;