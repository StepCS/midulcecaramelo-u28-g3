import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { APIInvoke } from '../../utils/ApiInvoke';
import swal from "sweetalert";
const api = new APIInvoke();

const NewAccount = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const [admin, setAdmin] = useState({
        admId: "",
        email: "",
        password: ""
    })

    const { admId, email, password } = admin;

    const onChange = (e) => {
        setAdmin({
            ...admin,
            [e.target.name]: e.target.value
        })
    };

    useEffect(() => {
        document.getElementById("admId").focus();
    }, []);

    const newAccount = async () => {
        const data = {
            idAdm: admin.admId,
            email: admin.email,
            password: admin.password
        }

        const response = await api.invokePOST("/api/UserAdm/new", data);
        console.log(response);
        const rest = response.msg;
        let titulo, msg, tipo;
        if (rest === "Admin Existente") {
            titulo = "Error al crear el admin";
            msg = "El correo ya existe";
            tipo = "error";
            alerta(msg, tipo, titulo);
        } else if (rest === "Admin creado") {
            titulo = "Proceso Exitoso!";
            msg = "Admin Creado correctamente";
            tipo = "success";
            alerta(msg, tipo, titulo);
        }
        setAdmin({
            admId: "",
            email: "",
            password: ""
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        newAccount();
    };



    return (

        <section className="fullPage-section p-0">
            <div className="full-page__wrapper">
            <div class="full-page__image">
                    <img src="https://mejorconsalud.as.com/wp-content/uploads/2017/10/4-Postres-caseros-nutritivos-y-con-muy-pocos-ingredientes-768x512.jpg" alt="comming-soon" />
                    <div class="contact__overlay">
                        <div class="contact__info">
                            <h2 class="section__title">Mi Dulce Caramelo</h2>
                            <p class="content">Ingreso administrativo.</p>
                            <div class="contact__meta">
                                <ul class="contact__meta__list">
                                    <li>
                                        <div class="icon">
                                            <svg width="26" height="32" viewBox="0 0 26 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M12.5817 16.9803C15.331 16.9803 17.5598 14.7515 17.5598 12.0021C17.5598 9.25278 15.331 7.02399 12.5817 7.02399C9.83231 7.02399 7.60352 9.25278 7.60352 12.0021C7.60352 14.7515 9.83231 16.9803 12.5817 16.9803Z"
                                                    fill="#0067FF"
                                                ></path>
                                                <path
                                                    d="M12.5818 0C5.6601 0 0 5.5919 0 12.5477C0 18.276 8.59243 28.2323 11.593 31.5397C12.1385 32.1534 13.0591 32.1534 13.6047 31.5397C16.6052 28.2323 25.1977 18.276 25.1977 12.5477C25.1636 5.52371 19.5035 0 12.5818 0ZM12.5818 20.9014C7.63772 20.9014 3.61428 16.878 3.61428 12.0021C3.61428 7.12627 7.63772 3.10282 12.5818 3.10282C17.5258 3.10282 21.5493 7.12627 21.5493 12.0021C21.5493 16.878 17.5258 20.9014 12.5818 20.9014Z"
                                                    fill="#0067FF"
                                                ></path>
                                            </svg>
                                        </div>
                                        <a href="tel:555-0129">3000000000</a>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <svg width="26" height="32" viewBox="0 0 26 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M12.5817 16.9803C15.331 16.9803 17.5598 14.7515 17.5598 12.0021C17.5598 9.25278 15.331 7.02399 12.5817 7.02399C9.83231 7.02399 7.60352 9.25278 7.60352 12.0021C7.60352 14.7515 9.83231 16.9803 12.5817 16.9803Z"
                                                    fill="#0067FF"
                                                ></path>
                                                <path
                                                    d="M12.5818 0C5.6601 0 0 5.5919 0 12.5477C0 18.276 8.59243 28.2323 11.593 31.5397C12.1385 32.1534 13.0591 32.1534 13.6047 31.5397C16.6052 28.2323 25.1977 18.276 25.1977 12.5477C25.1636 5.52371 19.5035 0 12.5818 0ZM12.5818 20.9014C7.63772 20.9014 3.61428 16.878 3.61428 12.0021C3.61428 7.12627 7.63772 3.10282 12.5818 3.10282C17.5258 3.10282 21.5493 7.12627 21.5493 12.0021C21.5493 16.878 17.5258 20.9014 12.5818 20.9014Z"
                                                    fill="#0067FF"
                                                ></path>
                                            </svg>
                                        </div>
                                        Colombia
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="full-page__inner">
                    <div className="full-page__content">
                        <div className="full-page__logo d-md-none d-bock">
                            <a href="home.html"><img src="assets/images/logo/logo.png" alt="logo" /></a>
                        </div>
                        <div className="popup__form d-block">
                            <form onSubmit={onSubmit} className="form__wrapper p-0">
                                <div className="popup__title">
                                    <h2>Crear cuenta administrativa</h2>
                                </div>
                                <div className="form-group">
                                <input 
                                            type="text" 
                                            className="form-control" 
                                            id="admId"
                                            placeholder="Id admin"
                                            name = "admId"
                                            value={admId}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingInput">Id Admin</label>
                                </div>
                                <div className="form-group">
                                <input 
                                            type="email" 
                                            className="form-control" 
                                            id="email"
                                            placeholder="Email"
                                            name='email'
                                            value={email}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingInput">Correo Electronico</label>
                                </div>
                                <div className="form-group">
                                <input 
                                            type="password" 
                                            className="form-control" 
                                            id="password"
                                            placeholder="Contraseña"
                                            name="password"
                                            value={password}
                                            onChange={onChange}
                                            required />
                                        <label htmlFor="floatingPassword">Contraseña</label>
                                </div>
                                <div className="submit__btn">
                                    <Link className="btn btn-primary mb-5 mt-0 w-100" type="submit" to={`/Admin/ProdList`}>Crear Cuenta</Link>
                                    <Link className="btn btn-primary mt-0 w-100" type="submit" to={`/Login`}>Ya tengo cuenta</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    )
}

export default NewAccount;