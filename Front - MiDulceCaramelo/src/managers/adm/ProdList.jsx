import React from "react";
import NavbarAdmin from "../../components/NavbarAdm";
import FooterAdm from "../../components/FooterAdm";
import SidebarAdm from "../../components/SidebarAdm";
import { useEffect, useState } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import { confirm } from "react-confirm-box"
const api = new APIInvoke();


const ListProd = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const [product, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])

    const delProduct = async (e, id) => {
        e.preventDefault();

        const confirmar = await confirm("¿Desea eliminar este registro?");
        let msj, titulo, tipo;

        if (confirmar) {
            const response = await api.invokeDELETE("/api/Product/del/" + id);
            console.log(response.mensaje);

            msj = "Producto eliminado correctamente";
            tipo = "success";
            titulo = "Proceso exitoso";
            alerta(msj, tipo, titulo);

            loadProduct();
        } else {
            msj = "No se ha eliminado el registro";
            tipo = "warning";
            titulo = "Advertencia";
            alerta(msj, tipo, titulo);
        }
    };

    const onSubmit = async (e, id) => {
        e.preventDefault();
        const param = document.getElementById("param").value;
        const consult = await api.invokeGET("/api/Product/list/" + param);
        let msj, tipo, titulo;

        if (consult.mensaje === "sin registros") {
            msj = "No se generaron registros en la consulta";
            tipo = "warning";
            titulo = "Información";
            alerta(msj, tipo, titulo);
            document.getElementById("param").value = "";
        }
        else {
            setProduct(consult);
            document.getElementById("param").value = "";
        }
    }

    return (
        <div className="main_container ml-5">
            <NavbarAdmin />
            <SidebarAdm />
            <main>
                <div className="main__container">
                    {/* Main Inner Wrapper Start */}
                    <div className="main__inner--wrapper">
                        {/* Product List Start */}
                        <div className="row">
                            <div className="col-12">
                                <div className="dashboard__header">
                                    <div className="d__header--left">
                                        <h2 className="title">Lista de productos</h2>
                                    </div>
                                    <div className="add__btn">
                                    <Link to={"/Admin/NewProduct"} className="btn btn-primary " id="botonNewP">Producto Nuevo</Link>
                                    </div>
                                </div>
                                <form onSubmit={onSubmit} >
                                    <div className="card-body shadow">
                                        <div className="table-responsive">
                                            <table className="table dashboard__table order-list">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" className="text-start">
                                                            <div className="heading">
                                                                IMAGEN
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                            <div className="heading">
                                                                ID PRODUCTO
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                        </th>
                                                        <th scope="col" className="text-start">
                                                            <div className="heading">
                                                                NOMBRE
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                        </th>
                                                        <th scope="col" className="text-start">
                                                            <div className="heading">
                                                                CATEGORIA
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                        </th>
                                                        <th scope="col" className="text-center col-sm-6 col-md-4">
                                                            <div className="heading">
                                                                DESCRIPCIÓN
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                        </th>
                                                        <th scope="col" className="text-center">
                                                            <div className="heading">
                                                                PRECIO
                                                                <div className="icon"><i className="fa-solid fa-arrow-down" /></div>
                                                            </div>
                                                        </th>
                                                        <th scope="col" className="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {
                                        product.map(
                                            item =>
                                                    <tr>
                                                        <td className="text-start">
                                                            <div className="product-group">
                                                                <div className="icon-image">
                                                                    <img src={item.img} alt={item.name} />
                                                                </div>
                                                                <div className="title">{item.idProd}</div>
                                                            </div>
                                                        </td>
                                                        <td className="text-wrap">{item.name}</td>
                                                        <td className="text-wrap">{item.category}</td>
                                                        <td className="text-wrap">{item.description}</td>
                                                        <td>{item.price}</td>
                                                        <td>
                                                            <div className="card__action">
                                                            <Link className="btn btn-outline-success mx-3"
                                                            to={`/Admin/UpdateProduct/${item._id}`} >
                                                            Modificar
                                                        </Link>
                                                        <button
                                                            className="btn btn-outline-secondary"
                                                            onClick={(e) => delProduct(e, item._id)}>
                                                            Eliminar
                                                        </button>
                                                                
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                        )}
                                                </tbody>
                                            </table>
                                        </div>
                                        {/* pagination start  */}
                                        <nav className="pagination__wrapper mt-0 py-10">
                                            <ul className="pagination">
                                                <li className="pagination__item">
                                                    <a className="page-link" href="#"><i className="fa-solid fa-angle-left" /></a>
                                                </li>
                                                <li className="pagination__item active"><a className="page-link" href="#">1</a></li>
                                                <li className="pagination__item"><a className="page-link" href="#">2</a></li>
                                                <li className="pagination__item"><a className="page-link" href="#">3</a></li>
                                                <li className="pagination__item"><a className="page-link dot" href="#">...</a></li>
                                                <li className="pagination__item"><a className="page-link" href="#">8</a></li>
                                                <li className="pagination__item"><a className="page-link" href="#">9</a></li>
                                                <li className="pagination__item"><a className="page-link" href="#">10</a></li>
                                                <li className="pagination__item">
                                                    <a className="page-link" href="#"><i className="fa-solid fa-angle-right" /></a>
                                                </li>
                                            </ul>
                                        </nav>
                                        {/* pagination end */}
                                    </div>
                                    </form>
                            </div>
                        </div>
                        {/* Product List End */}
                    </div>
                    {/* Main Inner Wrapper End */}
                </div>

            </main>
            <FooterAdm />
        </div>
    )

}

export default ListProd;