import React from "react";
import { Link } from 'react-router-dom';
import NavbarAdm from "../../components/NavbarAdm"
import FooterAdm from "../../components/FooterAdm"
import SidebarAdm from "../../components/SidebarAdm"
import { useEffect, useState } from 'react';
import swal from "sweetalert";
import { APIInvoke } from "../../utils/ApiInvoke";
// import { useNavigate } from "react-router-dom";
const api = new APIInvoke();




const NewProd = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    // const navigate = useNavigate();

    const [product, setProduct] = useState({
        idProd: "",
        category: "",
        description: "",
        img: "",
        name: "",
        price: ""
    });

    const { idProd, category, description, img, name, price } = product;

    const onChange = (e) => {
        setProduct({
            ...product,
            [e.target.name]: e.target.value
        });
    }

    useEffect(() => {
        document.getElementById("idProd").focus();
    }, []);

    const newProduct = async () => {

        const data = {
            idProd: product.idProd,
            category: product.category,
            description: product.description,
            img: product.img,
            name: product.name,
            price: product.price
        }

        const response = await api.invokePOST("/api/Product/new", data);
        console.log(response);
        const mensaje = response.mensaje;
        let msj, tipo, titulo;

        if (mensaje === "producto creado") {
            msj = "Producto guardado correctamente";
            tipo = "success";
            titulo = "Proceso exitoso";
            alerta(msj, tipo, titulo);

            setProduct({
                prodId: "",
                category: "",
                description: "",
                img: "",
                name: "",
                price: ""
            });
        }
        else if (mensaje === "producto existente") {
            msj = "Existe un producto con el mismo nombre";
            tipo = "error";
            titulo = "No se pudo guardar";
            alerta(msj, tipo, titulo);

        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        newProduct();
    };

    return (
        <div className="container">
            <NavbarAdm />
            <SidebarAdm />


            <div className="main__container">
                {/* Main Inner Wrapper Start */}
                <div className="main__inner--wrapper">
                    {/* Product List Start */}
                    <div className="row">
                        <div className="col-12">
                            <div className="dashboard__header">
                                <div className="d__header--left">
                                    <h2 className="title">Crear Producto</h2>
                                </div>
                            </div>
                            <form onSubmit={onSubmit} action="#" className="dashboard__form">
                                <div className="row">
                                    <div className="col-xxl-8">
                                        <div className="dashboard-block">
                                            <div className="dashboard-block__title">
                                                <h4>Información</h4>
                                            </div>
                                            <div className="form__wrapper form-padding">
                                                <div className="flex__form c-3">
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Nombre</label>
                                                        <input type="text" name="name" id="name" className="form-control" placeholder="Nombre del producto" value={name} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Categoria</label>
                                                        <input type="text" name="category" id="category" className="form-control" placeholder="Saludable/No saludable" value={category} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Id Producto</label>
                                                        <input type="text" name="idProd" id="idProd" className="form-control" placeholder="Id del producto" value={idProd} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="stock">Precio</label>
                                                        <input type="number" name="price" id="price" className="form-control" placeholder="Precio" value={price} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="slug">Url</label>
                                                        <input type="url" name="img" id="img" className="form-control" placeholder="http://" value={img} onChange={onChange} required />
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="description">Descripción</label>
                                                    <textarea name="description" className="description" id="description" cols={30} rows={4} placeholder="Descripción" value={description} onChange={onChange} required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="dashboard-block">

                                            <div className="btn__group flex-wrap">
                                                <Link className="btn btn-primary" type="submit" to={`/Admin/ProdList`}>Crear Producto</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    {/* Product List End */}
                </div>
                {/* Main Inner Wrapper End */}
            </div>
            <FooterAdm />
        </div>
    )

};

export default NewProd;