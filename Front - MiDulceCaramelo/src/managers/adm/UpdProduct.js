import React from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { APIInvoke } from "../../utils/ApiInvoke";
import { useNavigate } from "react-router-dom";
import NavbarAdm from "../../components/NavbarAdm"
import SidebarAdm from "../../components/SidebarAdm"
import FooterAdm from "../../components/FooterAdm"
import swal from "sweetalert";
const api = new APIInvoke();

const UpdateProduct = () => {

    const alerta = (mensaje, tipo, titulo) => {
        swal({
            title: titulo,
            text: mensaje,
            icon: tipo,
            buttons: {
                confirm: {
                    text: "Aceptar",
                    value: true,
                    visible: true,
                    className: "btn btn-secondary",
                    closeModal: true
                }
            }
        });
    }

    const navigate = useNavigate();
    const { id } = useParams();

    const [product, setProduct] = useState({
        idProd: "",
        category: "",
        description: "",
        img: "",
        name: "",
        price: ""
    });

    const { idProd, category, description, img, name, price } = product;

    useEffect(() => {
        const loadData = async () => {
            const response = await api.invokeGET("/api/Product/list/" + id);
            setProduct(response);
        }

        document.getElementById("idProd").focus();
        loadData();
    }, [id]);

    const onChange = (e) => {
        setProduct({
            ...product,
            [e.target.name]: e.target.value
        });
    }

    const updProd = async () => {

        const data = {
            idProd: product.idProd,
            category: product.category,
            description: product.description,
            img: product.img,
            name: product.name,
            price: product.price
        }

        const response = await api.invokePUT("/api/Product/act/" + id, data);
        console.log(response)
        let msj, tipo, titulo;

        if (response.mensaje === "Producto modificado") {
            msj = "El producto no se puede modificar";
            tipo = "warning";
            titulo = "Error en el proceso";
            alerta(msj, tipo, titulo);

            navigate("/Admin/ProdList");

        } else {
            msj = "Producto modificado";
            tipo = "success";
            titulo = "Felicitaciones";
            alerta(msj, tipo, titulo);

            navigate("/Admin/ProdList");
        }
    }
    const onSubmit = (e) => {
        e.preventDefault();
        updProd();
    }

    return (
        <div>

            <NavbarAdm />

            <SidebarAdm />


            <div className="main__container">
                {/* Main Inner Wrapper Start */}
                <div className="main__inner--wrapper">
                    {/* Product List Start */}
                    <div className="row">
                        <div className="col-12">
                            <div className="dashboard__header">
                                <div className="d__header--left">
                                    <h2 className="title">Crear Producto</h2>
                                </div>
                            </div>
                            <form onSubmit={onSubmit} action="#" className="dashboard__form">
                                <div className="row">
                                    <div className="col-xxl-8">
                                        <div className="dashboard-block">
                                            <div className="dashboard-block__title">
                                                <h4>Información</h4>
                                            </div>
                                            <div className="form__wrapper form-padding">
                                                <div className="flex__form c-3">
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Nombre</label>
                                                        <input type="text" name="name" id="name" className="form-control" placeholder="Nombre del producto" value={name} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Categoria</label>
                                                        <input type="text" name="category" id="category" className="form-control" placeholder="Saludable/No saludable" value={category} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="product_name">Id Producto</label>
                                                        <input type="text" name="idProd" id="idProd" className="form-control" placeholder="Id del producto" value={idProd} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="stock">Precio</label>
                                                        <input type="number" name="price" id="price" className="form-control" placeholder="Precio" value={price} onChange={onChange} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="slug">Url</label>
                                                        <input type="url" name="img" id="img" className="form-control" placeholder="http://" value={img} onChange={onChange} required />
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="description">Descripción</label>
                                                    <textarea name="description" className="description" id="description" cols={30} rows={4} placeholder="Descripción" value={description} onChange={onChange} required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="dashboard-block">

                                            <div className="btn__group flex-wrap">
                                                <button type="submit" className="btn btn-primary">Editar Producto</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    {/* Product List End */}
                </div>
                {/* Main Inner Wrapper End */}
            </div>

            <FooterAdm />
        </div>
    )

}

export default UpdateProduct;