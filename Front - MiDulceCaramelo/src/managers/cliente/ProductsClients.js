import React from "react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { APIInvoke } from "../../utils/ApiInvoke";
const api = new APIInvoke();

const ProductList = () => {

    const [products, setProduct] = useState([]);

    const loadProduct = async () => {
        const response = await api.invokeGET("/api/Product/list");
        console.log(response);
        setProduct(response);
    }

    useEffect(() => {
        loadProduct();
    }, [])

    return (
        <div className="product-card__wrapper">
            <div className="container">
                {
                    products.map (
                        product => 
                        <div className={product}>
                            <img src={product.img} alt={product.name} />
                            <div>
                                <p>
                                    {product.name} - ${product.price} - {product.description}
                                </p>
                            </div>
                        </div>
                    )}
            </div>
        </div>

    )
}
export default ProductList;