import React from "react";
import { Link, useNavigate } from "react-router-dom";

const Navbar = () => {
  const navigate = useNavigate();

  return (
    <div class="header__dashboard">
      <div class="header__left">
        <div className="header__logo">
          <a href="home.html">
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhIf81NburFfqoGMwXx_FVlQsxQBKpQSBwSQ&usqp=CAU"
              alt="logo"
            />{" "}
          </a>
        </div>
      </div>

      <div class="header__right">
        <div className="card__action">
          <Link
            className="btn btn-primary"
            to={`/Home`}
          >
            Salir
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
