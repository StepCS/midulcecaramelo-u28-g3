import React from "react";
import { Link } from "react-router-dom";

const FooterAdmin = () => {
    return (
        
                <footer className="footer__section">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 col-md-12 col-sm-12">
                                <div className="footer__wrapper">
                                    <div className="footer__widget d-block">
                                        <p>MiDulceCaramelo  se establece como una empresa líder
                                            en la elaboración de exquisitos postres, a través de
                                            productos 100% colombianos cuidando la salud y
                                            bienestar de los tuyos.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-12">
                                <div className="footer__wrapper">
                                    <div className="footer__widget">
                                        <div className="title">
                                            <h5>Productos</h5>
                                        </div>
                                        <ul>
                                            <Link className="text-muted" to={`/products`}>Conoce nuestros productos</Link>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-12">
                                <div className="footer__wrapper">
                                    <div className="footer__widget">
                                        <div className="title">
                                            <h5>Cotización</h5>
                                        </div>
                                        <ul>
                                            <Link className="text-muted" to={`/products`}>Cotizar productos</Link>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                {/* Accordion Start */}
                                <div className="accordion__wrapper">
                                    <div className="accordion accordion-flush" id="accordionFlushExample01">
                                        <div className="accordion-item">
                                            <h2 className="accordion-header" id="flush-heading01">
                                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse01" aria-expanded="false" aria-controls="flush-collapse01">
                                                    Product
                                                </button>
                                            </h2>
                                            <div id="flush-collapse01" className="accordion-collapse collapse" aria-labelledby="flush-heading01" data-bs-parent="#accordionFlushExample01">
                                                <div className="accordion-body">
                                                    <div className="footer__widget">
                                                        <ul>
                                                            <li><a href="#">Shirt</a></li>
                                                            <li>
                                                                <a href="#">Groceries <span className="danger">Hot</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Computer <span>New</span></a>
                                                            </li>
                                                            <li><a href="#">Electronics</a></li>
                                                            <li><a href="#">Sports</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item">
                                            <h2 className="accordion-header" id="flush-heading02">
                                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse02" aria-expanded="false" aria-controls="flush-collapse02">
                                                    Company
                                                </button>
                                            </h2>
                                            <div id="flush-collapse02" className="accordion-collapse collapse" aria-labelledby="flush-heading02" data-bs-parent="#accordionFlushExample01">
                                                <div className="accordion-body">
                                                    <div className="footer__widget">
                                                        <ul>
                                                            <li><a href="#">About us</a></li>
                                                            <li><a href="#">Careers</a></li>
                                                            <li><a href="#">News</a></li>
                                                            <li><a href="#">Media kit</a></li>
                                                            <li><a href="#">Contact</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item">
                                            <h2 className="accordion-header" id="flush-heading03">
                                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse03" aria-expanded="false" aria-controls="flush-collapse03">
                                                    Resources
                                                </button>
                                            </h2>
                                            <div id="flush-collapse03" className="accordion-collapse collapse" aria-labelledby="flush-heading03" data-bs-parent="#accordionFlushExample01">
                                                <div className="accordion-body">
                                                    <div className="footer__widget">
                                                        <ul>
                                                            <li><a href="#">Blog</a></li>
                                                            <li>
                                                                <a href="#">Newsletter <span>Best</span></a>
                                                            </li>
                                                            <li><a href="#">Help centre</a></li>
                                                            <li><a href="#">Tutorials</a></li>
                                                            <li><a href="#">Support</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="accordion-item">
                                            <h2 className="accordion-header" id="flush-heading04">
                                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse04" aria-expanded="false" aria-controls="flush-collapse04">
                                                    Legal
                                                </button>
                                            </h2>
                                            <div id="flush-collapse04" className="accordion-collapse collapse" aria-labelledby="flush-heading04" data-bs-parent="#accordionFlushExample01">
                                                <div className="accordion-body">
                                                    <div className="footer__widget">
                                                        <ul>
                                                            <li><a href="#">Terms</a></li>
                                                            <li><a href="#">Privacy</a></li>
                                                            <li><a href="#">Licenses</a></li>
                                                            <li><a href="#">Settings</a></li>
                                                            <li><a href="#">Contact</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Accordion End */}
                            </div>
                        </div>
                    </div>
                    <div className="footer__bottom">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <div className="footer__content text-center">
                                        <div className="content">
                                            <p>
                                                © Grupo 6 U28 - Desarrollo Web
                                                <a href="#"> MiDulceCaramelo</a>. All rights reserved
                                            </p>
                                        </div>
                                        <div className="link">
                                            <a href="#"><i className="fa-brands fa-twitter" /> </a>
                                            <a href="#"><i className="fa-brands fa-linkedin" /></a>
                                            <a href="#"><i className="fa-brands fa-facebook" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
        
        
            )
        }
        
export default FooterAdmin;