const { Router } = require("express");

const UserAdmRouter = Router();
const UserAdmCtrl = require("../controllers/UserAdmCtrl");

UserAdmRouter.post("/new", UserAdmCtrl.admSave);
UserAdmRouter.post("/login", UserAdmCtrl.admLogin);
UserAdmRouter.get("/list", UserAdmCtrl.admList);
UserAdmRouter.get("/list/:id", UserAdmCtrl.admId);
UserAdmRouter.delete("/del/:id", UserAdmCtrl.admDel);
UserAdmRouter.put("/act/:id", UserAdmCtrl.admMod);

module.exports = UserAdmRouter;
