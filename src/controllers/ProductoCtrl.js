const Producto = require("../models/Productos");
const jwt = require("jsonwebtoken");


const prodSave = async (req, res) => {
    try {
        const {idProd,category,description,img,name,price} = req.params;

        let product = await Producto.findOne({idProd});
        if(product){
            return res.status(400).json({ mensaje : "Producto existente"});
        }else {
            product = new Producto(req.body);
            await product.save();
            return res.status(200).json({ mensaje : "Producto creado"});
        }

        const payload={
            product:{id: product.id }
        };

        jwt.sign(
            payload,
            process.env.SECRETA,
            {
                expiresIn : 3600,
            },
            (error, token) =>{
                if (error) throw error;
                res.json({token})
            });
    }catch(error){
        console.error(error);
    }

}

const producList = async (req, res) => {
    try {
        let prodList = await Producto.find();
        res.status(200).send(prodList)
    } catch (error) {
        console.error(error);
    }
}

const prodId = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let prod = await Producto.findById(id);
            res.status(200).send(prod);
        }
        else{
            res.send("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const prodDel = async (req, res) => {
    try {
        const id = req.params.id;
        if(id){
            let prod = await Producto.findByIdAndDelete(id);
            res.status(200).json("Producto eliminado correctamente");
        }
        else{
            res.status(400).json("No se puede tramitar la solicitud");
        }
    } catch (error) {
        console.error(error);
    }
}

const prodMod = async (req, res) => {
    try {
        const id = req.params.id;
        const producto = req.body;
        await Producto.findByIdAndUpdate(id, producto);
        res.status(200).json({mensaje:"Producto actualizado correctamente"});

        const payload = {
            producto: {id: producto.id},
        };

        jwt.sign(
            payload,
            process.env.SECRETA,{
                expiresIn: 3600, //1 hora
            },
            (error, token) => {
                if (error) throw error;

                //Mensaje de confirmación
                res.json({ token });
            }
        )

    }catch (error) {
        console.error(error);
    }
}

const prodParam = async (req, res) => {
    try {
        const param = req.params.param;
        const product = await Producto.find({
            $or: [
            {idProd: { $regex: '.*' + param + '.*' }},
            {name: { $regex: '.*' + param + '.*' }},
            {category: { $regex: '.*' + param + '.*' }}
        ]
        });
        res.status(200).json({mensaje: "No existe"})
    }catch (error) {
        console.error(error);
    }
}

module.exports = {
    prodSave,
    producList,
    prodId,
    prodDel,
    prodMod,
    prodParam
}