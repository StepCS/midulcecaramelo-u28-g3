const mongoose = require("mongoose")
const autoIncrement = require('mongoose-auto-increment');

let conect = mongoose.createConnection(process.env.DB_MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
autoIncrement.initialize(conect);

const cotizacionSchema = new mongoose.Schema(
    {
        address: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        nameUs: {
            type: String,
            required: true,
        },
        phone: {
            type: String,
            required: true,
        },
        orderId: {
            type: Number,
            unique: true,
        },
        producto: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Producto',
            autopopulate: true
        }],
        amount: {
            type: Number,
        },
        total: {
            type: Number,
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
);


cotizacionSchema.plugin(require('mongoose-autopopulate'));
cotizacionSchema.plugin(autoIncrement.plugin, {
    model: "Cotizacion",
    field : "orderId",
    startAt: 100,
    increment: 1});

const Cotiz = new mongoose.model("Cotizacion", cotizacionSchema);
module.exports = Cotiz;